### Keybase proof

I hereby claim:

  * I am brjsp on github.
  * I am brjsp (https://keybase.io/brjsp) on keybase.
  * I have a public key whose fingerprint is 26CF A02D 1C06 92ED E592  DF78 32B7 5669 7793 5F75

To claim this, I am signing this object:

```json
{
  "body": {
    "key": {
      "eldest_kid": "0101cd58bfbf2579f028fbc7f00753d03415ee4e0c1910d70854b5f25326b4ce31710a",
      "fingerprint": "26cfa02d1c0692ede592df7832b7566977935f75",
      "host": "keybase.io",
      "key_id": "32b7566977935f75",
      "kid": "0101cd58bfbf2579f028fbc7f00753d03415ee4e0c1910d70854b5f25326b4ce31710a",
      "uid": "2e4b0d4fc28213c96dfc6f712a141219",
      "username": "brjsp"
    },
    "merkle_root": {
      "ctime": 1611936166,
      "hash_meta": "7188ad73535c6d7d86ddb4ebfc16e64c3e999a305a745a5f373dc9e09236f09d",
      "seqno": 18947777
    },
    "revoke": {
      "sig_ids": [
        "1cc56694622ffc3c92fcd80ae617eeccae1787b1355e8966ee17756b71569d2e0f"
      ]
    },
    "service": {
      "name": "github",
      "username": "brjsp"
    },
    "type": "web_service_binding",
    "version": 1
  },
  "ctime": 1611936168,
  "expire_in": 157680000,
  "prev": "2950037e4f82067db06fcdceafe09f44ae3dfb9272f1da071b10d25c257ad4ea",
  "seqno": 3,
  "tag": "signature"
}
```

with the key [26CF A02D 1C06 92ED E592  DF78 32B7 5669 7793 5F75](https://keybase.io/brjsp), yielding the signature:

```
-----BEGIN PGP MESSAGE-----

owGtU3tQVFUcXhBJUQLSLB9lXUszNzrnvs69q1OWiQ7OWmL5CG279zzwCuzCvbsL
pvjCHEwqQctRFMXJGSa1ycFBmXxmgInppKVmiqCiWTHqhM4qZZ1ldJpp6r/OP3d+
537f7/d93zlnRWI3V48YcecUq2ClLxTTdNV0vd0Xfj5XMANkjuCZK2TTrg/NIdQJ
+rItIngEAAHERNFMZjJRQToDosZMjBgASJEIkGSoUCpTgKEOAUFAU2RT4VBJVE0Z
UwkiCAzBLTDLn0XtPNvyB3lbUcXMACKBGKi6SAlVdJEwpEmiiRRV1RHSJYUhhRNn
BZwog4szDYemWgG+xwtfl7x/wf/PukNd7UQqm4DIDIuaCCWsq4RhlSEoGlCGItSj
QIfafiOXcrRpz3byhCK3kEvt7BzqswOBYDRZHLSi/6EKoS6pUFW5O8OZ5culQYPT
ENQ0gyBJkRSsEkQ0lRBTpibDUKWqjCWq67ohAcVAsmIoTEISwToFuiipDOiEi3Bo
vj/AJ2i6jPjiEmwaDmTT6HTHyuKhOYInU4AY89T2W7qsiiJjmDsSGSYaMKgKEaUY
GxQiDZlQUhSq6apKec2TNhFUVJ2IFDBhZlF0nh22cFf7e96zrOCskPkfeQTn5EXr
Amr67jF9puUn/GpwQpjajhXwc/Uc+c+oNLdAC/Msm/qsKEJBqgb4cgt53GD0gHQF
AAlRmWkiUBExgcoNYWowng+TZYNKhJm6iEQGiQEQNPmpiwrmd8MgMjX+zk7iMo0s
3pLn5TeCIZsKRQnLYqU4V0wP12P9nowbemzjc4Piy9vbys8X3n9N3WOjT8mV0DP5
/s74F5NdDSSCxlQl9y4rizxfPVI68eaRr1YX1k+83cuXvJqNKjq3r/DVd/u05N/1
DKzPSd1e2iuf4U9ulK24KP2YNt0/svHTgxemNr/uPZ14aULW9988Eb6UXLfDak8Y
1D91ztGK3rggMzDqUOVduWNP2u7q9rzK0puvrO1Mr3zntTsNx1fVmAnvZRRMnrsn
KT7Ou3d/581Jv8/f27Jh0q2TtW9sXNK266x36egZR9YO9z50OumllvZIdTh23fkv
z329Y0x6xlsL5T8W95s8rjZNnHLCPb+WFMvNGxYBOyV2cWv8wdgrfaddGna24s7A
UHhqfdOqrSktzuGkTrZzQEHj05dzQlc7ir0D5AkdS6ezM+u7nS6JrzlXNnjKtp/7
1m96fND7iTVx12vFNLb2wbRTjTHPDp58Ztdn/YcNXJNRevmLreNL+pR817pvmIdo
2c+g5sjsBwKHYOgXu6NoyLZb1tVjnvOTtvx04NHN4Zczuj/su95gH2/zbk9YdCxp
6IhrMw+4lZQhVfr8BQtWRg7MKLHq75a15lxxXAuD7vLI2KZfpeVVw4sadow9tFI+
uSV/RKSm0B619eiatrqPrfX+k91vVmRezv9gSaP7o2sfNidt3jsxZtqpb9Nzn7q4
rqnYXW7MuNBj0+3D8zbtHlpXLs0rrbzmTb3hyvntkRu9/jw7r2dV/UV3a2unpy4x
c/kPy19YCKuXVcQ64/4C
=HcNN
-----END PGP MESSAGE-----

```

And finally, I am proving ownership of the github account by posting this as a gist.

### My publicly-auditable identity:

https://keybase.io/brjsp

### From the command line:

Consider the [keybase command line program](https://keybase.io/download).

```bash
# look me up
keybase id brjsp
```
